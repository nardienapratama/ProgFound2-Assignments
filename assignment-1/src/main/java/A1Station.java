import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    // You can add new variables or methods in this class

    public static void main(String[] args) {
        // TODO Complete me!
        // Create a new Scanner object to read input from standard input
        Scanner input = new Scanner(System.in);
        int value = Integer.parseInt(input.nextLine());    //accepts input from standard input
                                        //specifies the no. of cats that will be processed by the program
        TrainCar firstcar = null;       // there is no car yet
        int numtrain = 0;

        for(int i=0; i < value; i++){
            String[] catinputname = input.nextLine().split(",");   // input cat's name, weight, and length
            String temp_name = catinputname[0];
            double temp_weight = Double.parseDouble(catinputname[1]);
            double temp_length = Double.parseDouble(catinputname[2]);
            WildCat catdata = new WildCat(temp_name, temp_weight, temp_length);  // puts the value from the variables into wildcat   
                 

            if(firstcar == null){
                TrainCar newtraincar = new TrainCar(catdata);                  // puts the cat's data from the previous line
                firstcar = newtraincar;                                // inputs data from newtrain to firstcar
                numtrain++;                                     // adds 1 to the numtrain
            }
            else{
                TrainCar newtraincar = new TrainCar(catdata, firstcar);        // inputs the catdata and newtrain vars into the traincar method - it will always go through the first 'if' since newtrain is set to 0
                firstcar = newtraincar;                                 // new car replaces the firstcar - it becomes the head
                numtrain++;                                     // adds 1 to the numtrain
            }

            if(firstcar.computeTotalWeight()>THRESHOLD || i == (value-1)){
                System.out.println("The train departs to Javari Park");
                System.out.print("[LOCO]<");
                firstcar.printCar();            // p
                System.out.println();           // prints new line

                double averagemassindex = firstcar.computeTotalMassIndex()/(double)numtrain;
                System.out.println("Average mass index of all cats: " + String.format("%.2f", averagemassindex));       // weight of the first car divided by the number of cats = average mass index of each cat
            
                // if loops for determining the label based on the average mass index
                String category;
                if(averagemassindex < 18.5){
                    category = "underweight";
                }
                else if(18.5 <= averagemassindex && averagemassindex < 25){
                    category = "normal";
                }
                else if(25 <= averagemassindex && averagemassindex< 30){
                    category = "overweight";
                }
                else{
                    category = "obese";
                }

                System.out.println("In average, the cats in the train are " + "*" + category + "*");
                                
                firstcar = null;
                numtrain = 0;
            }

                                    
        }      
            
    }
}                                