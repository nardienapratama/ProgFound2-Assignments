import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class Card extends JButton{
    public static final Icon bgImage = new ImageIcon("background.png");
    private int id;
    private Icon mainImage;
    private boolean matched = false;

     public Card(int value, String image){
         super(bgImage);
         this.setId(value);
         this.mainImage = new ImageIcon(image);
         Image rescaled = ((ImageIcon)this.mainImage).getImage().getScaledInstance(90,90, java.awt.Image.SCALE_SMOOTH);
         this.mainImage = new ImageIcon(rescaled);
         this.setPreferredSize(new Dimension(100,100));
     }

    public void setId(int id){
        this.id = id;
    }
    public int getId(){
        return this.id;
    }
    public void setMatched(boolean truth){
        this.matched = truth;
    }
    public boolean getMatched(){
        return this.matched;
    }

    public void showText(){
        this.setText(String.valueOf(id));
    }

    public Icon getMainImage() {
        return mainImage;
    }

    public void setMainImage(Icon mainImage) {
        this.mainImage = mainImage;
    }


}