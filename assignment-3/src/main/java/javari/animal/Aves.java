package javari.animal;
public class Aves extends Animal{

    private boolean laying;
    private String specialcond;

    public Aves(Integer id, String type, String name, Gender gender, double length, 
    double weight, String specialcond, Condition condition){
        super(id, type, name, gender, length, weight, condition);
        this.specialcond = specialcond;
        this.laying = false;
    }

    // to check their conditions - determines whether they can do attractions
    protected boolean specificCondition(){
        if(specialcond.equalsIgnoreCase("laying")){
            this.laying = true; // cannot do attraction
        }return this.laying; // returns these boolean values 
    }



}