import java.util.Scanner;

public class Javari{

    public static void main(String[] args) {
        // TODO Complete me!
        // Create a new Scanner object to read input from standard input
        Scanner input = new Scanner(System.in);
        //int numanimal = Integer.parseInt(input.nextLine());   

        
        System.out.println("Welcome to Javari Park!");
        System.out.println("Input the number of animals");
        System.out.print("cat: ");
        int catnumber = Integer.parseInt(input.nextLine());     // how many cats put right next to the previous string
        Cat[] catobjects = new Cat[catnumber];
        if(catnumber > 0 ){
            System.out.println("Provide the information of cat(s): ");
            String catinfo = input.nextLine();          // name of each cat and length
            String[] catinfonew = catinfo.split(",");   // [Ana|30, Ringgo|78]
            for(int i =0; i<catnumber; i++){
                catobjects[i] = new Cat(catinfonew[i].split("\\|")[0], Integer.parseInt(catinfonew[i].split("\\|")[1]));  //[[Ana, 30], [Ringgo, 78]] 
                //catagearray[i] = (catinfonew[i].split("\\|"));
                // new Cat(Ana, 30) -----> catobjects[0]=[ana(isi properties and methods of cat for ana)]
            }
        }

        System.out.print("lion: ");
        int lionnumber = Integer.parseInt(input.nextLine());    
        Lion[] lionobjects = new Lion[lionnumber];
        if(lionnumber > 0){
            System.out.println("Provide the information of lion(s): ");
            String lioninfo = input.nextLine();
            String[] lioninfonew = lioninfo.split(",");
            for(int i =0; i<lioninfonew.length; i++){
                lionobjects[i] = new Lion(lioninfonew[i].split("\\|")[0], Integer.parseInt(lioninfonew[i].split("\\|")[1]));                
                
            }
        }

        System.out.print("eagle: ");
        int eaglenumber = Integer.parseInt(input.nextLine());
        Eagle[] eagleobjects = new Eagle[eaglenumber];
        if(eaglenumber > 0){
            System.out.println("Provide the information of eagle(s): ");
            String eagleinfo = input.nextLine();
            String[] eagleinfonew = eagleinfo.split(",");
            for(int i =0; i<eagleinfonew.length; i++){
                eagleobjects[i] = new Eagle(eagleinfonew[i].split("\\|")[0], Integer.parseInt(eagleinfonew[i].split("\\|")[1]));                
                
            }
        }

        System.out.print("parrot: ");
        int parrotnumber = Integer.parseInt(input.nextLine());
        Parrot[] parrotobjects = new Parrot[parrotnumber];
        if(parrotnumber > 0){
            System.out.println("Provide the information of parrot(s): ");
            String parrotinfo = input.nextLine();
            String[] parrotinfonew = parrotinfo.split(",");
            for(int i =0; i<parrotinfonew.length; i++){
                parrotobjects[i] = new Parrot(parrotinfonew[i].split("\\|")[0], Integer.parseInt(parrotinfonew[i].split("\\|")[1]));                
               
            }
        }    

        System.out.print("hamster: ");
        int hamsternumber = Integer.parseInt(input.nextLine());
        Hamster[] hamsterobjects = new Hamster[hamsternumber];
        if(hamsternumber > 0){
            System.out.println("Provide the information of hamster(s): ");
            String hamsterinfo = input.nextLine();
            String[] hamsterinfonew = hamsterinfo.split(",");
            for(int i =0; i<hamsterinfonew.length; i++){
                hamsterobjects[i] = new Hamster(hamsterinfonew[i].split("\\|")[0], Integer.parseInt(hamsterinfonew[i].split("\\|")[1]));                
               
            }
        }    


        System.out.println("Animals has been successfully recorded!" +  
        "\n===================================================");

        Animals[][] animalobjects = new Animals[5][];       // for for-looping cage
        animalobjects[0] = catobjects;
        animalobjects[1] = lionobjects;
        animalobjects[2] = eagleobjects;
        animalobjects[3] = parrotobjects;
        animalobjects[4] = hamsterobjects;


        //for(int i=0; i <  )


        //this is where you put the cage arrangements
        
        System.out.println("Cage arrangement:");
        for(int i = 0; i<5; i++){
            if(animalobjects[i].length !=0){
                CageArrangement x = new CageArrangement(animalobjects[i]);  // the array of animals is put in the CageArrangement class and method
                System.out.println("location: " + Cage.determineABC(animalobjects[i][0]).getLocation());
                System.out.println(x.printInfo());  //applies x into the printinfo method and prints the return result
                x.arrangearray();    //puts x into the arrange method
                System.out.println("\nAfter rearrangement...");
                System.out.println(x.printInfo());  //prints after rearrangement

            }
        }


        System.out.println("ANIMALS NUMBER: \ncat:" + catnumber +
                            "\nlion: " + lionnumber + 
                            "\nparrot: " + parrotnumber +
                            "\neagle: " + eaglenumber +
                            "\nhamster: " + hamsternumber + "\n\n"+
                            "\n===================================================");

        while(true){
            System.out.println("Which animal you want to visit? \n" +
                            "1: Cat, 2: Eagle, 3: Hamster, 4: Parrot," +
                            " 5: Lion, 99: exit)");
            int animalinput = Integer.parseInt(input.nextLine());
            
            // if animalinput is cat/1
            if(animalinput == 1){
                System.out.print("Mention the name of cat you want to visit: ");
                String nameofcat = input.nextLine();
                boolean catfound = false;
                for(int i=0; i<catobjects.length; i++){
                    if(catobjects[i].getName().equals(nameofcat)){
                        catfound = true;  
                        System.out.println("You are visiting " + catobjects[i].getName() + " (cat)" +
                        " now, what would you like to do?\n" +
                        "1: Brush the fur 2: Cuddle");

                        int catmethodinput = Integer.parseInt(input.nextLine());
                        if(catmethodinput == 1){
                            System.out.println(catobjects[i].brushCatFur());          // applies it into this method
                        }
                        else if(catmethodinput == 2){
                            System.out.println(catobjects[i].cuddle());
                        }
                        else{
                            System.out.println("You do nothing...\nBack to the office!\n\n");
                        }
                        break;
                    }
                }

                // if  the cat is not found....:
                if(!catfound) {
                    System.out.println("There is no cat with that name! Back to the office!\n");
                    System.out.println(" ");
                }
            }

            //if animal input is Eagle/2
            else if(animalinput == 2){
                System.out.print("Mention the name of eagle you want to visit: ");
                String nameofeagle = input.nextLine();
                boolean eaglefound = false;
                for(int i=0; i<eagleobjects.length; i++){
                    if(eagleobjects[i].getName().equals(nameofeagle)){
                        eaglefound = true;  
                        System.out.println("You are visiting " + eagleobjects[i].getName() + " (eagle) " +
                                        "now, what would you like to do?\n" +
                                        "1: Order to fly");
                        int eaglemethodinput = Integer.parseInt(input.nextLine());
                        if(eaglemethodinput == 1){
                            System.out.println(eagleobjects[i].orderToFly());          // applies it into this method
                        }
                        else{
                            System.out.println("You do nothing...\nBack to the office!\n\n");
                        }
                        break;
                    }
                }
                if(!eaglefound){
                    // if  the eagle is not found....:
                    System.out.println("There is no eagle with that name! Back to the office!");
                    System.out.println(" ");
                }
                
            }
                

            // if animalinput is hamster/3:
            else if(animalinput == 3){
                System.out.print("Mention the name of hamster you want to visit: ");
                String nameofhamster = input.nextLine();
                boolean hamsterfound = false;
                for(int i=0; i<hamsterobjects.length; i++){
                    if(hamsterobjects[i].getName().equals(nameofhamster)){
                        hamsterfound = true;  
                        System.out.println("You are visiting " + hamsterobjects[i].getName() + " (hamster) " +
                                        "now, what would you like to do?\n" +
                                        "1: See it gnawing 2: Order to run in the hamster wheel");
                        int hamstermethodinput = Integer.parseInt(input.nextLine());
                        if(hamstermethodinput == 1){
                            System.out.println(hamsterobjects[i].seeGnawing());          // applies it into this method
                        }
                        else if(hamstermethodinput == 2){
                            System.out.println(hamsterobjects[i].orderToRunInWheel());
                        }
                        else{
                            System.out.println("You do nothing...\nBack to the office!\n\n");
                        }
                        break;
                    }
                }
                if(!hamsterfound){
                    // if  the hamster is not found....:
                    System.out.println("There is no hamster with that name! Back to the office!");
                    System.out.println(" ");
                }
                
            }   
            

            // if animalinput is parrot/4:
            else if(animalinput == 4){
                System.out.print("Mention the name of parrot you want to visit: ");
                String nameofparrot = input.nextLine();
                boolean parrotfound = false;
                for(int i=0; i<parrotobjects.length; i++){
                    if(parrotobjects[i].getName().equals(nameofparrot)){
                        parrotfound = true;  
                        System.out.println("You are visiting " + parrotobjects[i].getName() + " (parrot) " +
                                        "now, what would you like to do?\n" +
                                        "1: Order to fly 2: Do conversation");
                        int parrotmethodinput = Integer.parseInt(input.nextLine());
                        if(parrotmethodinput == 1){
                            System.out.println(parrotobjects[i].orderToFly());          // applies it into this method
                        }
                        else if(parrotmethodinput == 2){
                            parrotobjects[i].doConvo();
                        }
                        else{
                            System.out.println(parrotobjects[i].doNothing());
                        break;
                        }
                    }
                }
                if(!parrotfound){
                // if  the parrot is not found....:
                System.out.println("There is no parrot with that name! Back to the office!\n");
                System.out.println(" ");
                }  
            }  

            // if animalinput is lion/5
            else if(animalinput == 5){
                System.out.print("Mention the name of lion you want to visit: ");
                String nameoflion = input.nextLine();
                boolean lionfound = false;
                for(int i=0; i<lionobjects.length; i++){
                    if(lionobjects[i].getName().equals(nameoflion)){
                        lionfound = true;  
                        System.out.println("You are visiting " + lionobjects[i].getName() + " (lion) " +
                                        "now, what would you like to do?\n" +
                                        "1: See it hunting 2: Brush the mane 3: Disturb it");
                        int lionmethodinput = Integer.parseInt(input.nextLine());
                        if(lionmethodinput == 1){
                            System.out.println(lionobjects[i].hunting());          // applies it into this method
                        }
                        else if(lionmethodinput == 2){
                            System.out.println(lionobjects[i].brushLionMane());
                        }
                        else if(lionmethodinput == 3){
                            System.out.println(lionobjects[i].disturb());
                        }
                        else{
                            System.out.println("You do nothing...\nBack to the office!\n\n");
                        break;
                        }
                    }
                }
                if(!lionfound){
                    // if  the lion is not found....:
                    System.out.println("There is no lion with that name! Back to the office!");
                    System.out.println(" ");
                } 
            
                
            }
            else if(animalinput == 99){
                break;
            } 
            else{
                System.out.println("Invalid input.");
            }
        }     
    }
}
