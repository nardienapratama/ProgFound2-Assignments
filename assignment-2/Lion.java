public class Lion extends Animals{

    String name;
    int length;

    public Lion(String name, int length){
        super(name, length, false);        // calls super class
    }

    public String hunting(){
        return "Lion is hunting.. \n" + 
                getName() + " makes a voice: err...! \n" +
                "Back to the office!\n\n"; 
    }

    public String brushLionMane(){
        return "Clean the lion's mane.. \n" + 
                getName() + " makes a voice: Hauhhmm! \n" +
                "Back to the office!\n\n"; 
    
    }
        
    public String disturb(){
        return getName() + " makes a voice: HAUHHMM! \n" +
                "Back to the office!\n\n";
    }

    
}