package javari.animal;

public class Reptiles extends Animal{

    private boolean tame;
    private String specialcond;

    public Reptiles(Integer id, String type, String name, Gender gender, double length, 
    double weight, String specialcond, Condition condition){
        super(id, type, name, gender, length, weight, condition);
        this.specialcond = specialcond;
        this.tame = false;
    }

    // to check their conditions - determines whether they can do attractions
    protected boolean specificCondition(){
        if(specialcond.equalsIgnoreCase("tame")){
            this.tame = true; // cannot do attraction
        }return this.tame; // returns these boolean values 
    }



}