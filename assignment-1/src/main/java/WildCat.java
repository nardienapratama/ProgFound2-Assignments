public class WildCat {

    // TODO Complete me!
    String name;    // name of the cat 
    double weight; // In kilograms
    double length; // In centimeters

    //constructor method that accepts 3 arguments
    public WildCat(String name, double weight, double length) {
        // TODO Complete me!
        this.name = name;
        this.weight = weight;  
        this.length = length;   // keyword 'this' refers to the attribute/method for an object in this class
    }

    public double computeMassIndex() {
        // TODO Complete me!
        double BMI = this.weight/((this.length/100)*(this.length/100));
        return BMI;
    }
}
