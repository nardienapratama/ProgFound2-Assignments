import java.util.ArrayList;

public class CageArrangement{
    ArrayList[] cagecontents = new ArrayList[3];    // [ <Arraylist1> , <Arraylist2> , <Arraylist3> ]
    Animals[] animalArray;
    public CageArrangement(Animals[] animalArray){
        int animalarrsize = animalArray.length;
        for(int i=0; i<3; i++) this.cagecontents[i] = new ArrayList<Cage>();    // [ <ArrayList1<Cage>> , <ArrayList2<Cage>> , <ArrayList3<Cage>>]
        for(int j=0; j<animalarrsize; j++){
            if(animalarrsize > 2){
                if(j < animalarrsize/3){
                    this.cagecontents[2].add(Cage.determineABC(animalArray[j]));   // level 1 cage - take the animals from  the input 'animal array' index 0 --- index n and put them in the arraylist1
                }
                else if(j < animalarrsize/3*2){
                    this.cagecontents[1].add(Cage.determineABC(animalArray[j]));   // level 2 cage - take the animals from  the input 'animal array' index 0 --- index n and put them in the arraylist2
                }
                else{
                    this.cagecontents[0].add(Cage.determineABC(animalArray[j]));   // level 3 cage - take the animals from  the input 'animal array' index 0 --- index n and put them in the arraylist3

                }
            }
            else{
                for(int k=0; k < animalarrsize; k++){
                    this.cagecontents[2-k].add(Cage.determineABC(animalArray[k]));
                }
                break;
            }
        }
    }
    public String printInfo(){
        String output = "";
        Cage x;
        for(int i=0; i< cagecontents.length; i++){
           output += "level " + (3-i) + ":";    // level 3:
           for(int j=0; j< cagecontents[i].size(); j++){
                x = (Cage)(cagecontents[i].get(j));
                output += " " + x.getAnimal().getName() + " (" + x.getAnimal().getLength() + " - " + x.getType() + "),";  
           }
           output += "\n";
        }
        return output;

    }
    public void arrangearray(){
        ArrayList temporary = new ArrayList<Cage>();
        for(int i=0; i<2; i++){
            temporary = this.cagecontents[(i)];  // level 3 --> temp
            this.cagecontents[i] = this.cagecontents[i+1];  //level 2--> level 3
            this.cagecontents[i+1] = temporary;  // temp(level 3) ---> level 2
        }

        ArrayList temporary2 = new ArrayList<Cage>();
        for(int i=0; i< cagecontents.length; i++){
            int size = this.cagecontents[i].size(); // how many animals are in it
            for(int j=0; j<size; j++){
                temporary2.add(0, this.cagecontents[i].get(j)); // get the first animal from level [i] and puts it in the front, so that it will all be reversed
            }
            this.cagecontents[i].clear();   // erases the previous levels (the one with correct order of levels, but wrong order of animals)
            for(int k=0; k< size; k++){
                this.cagecontents[i].add(temporary2.get(k));     // adds the array list with correct animal order and replaces the just-deleted array list
            }
        }
    }
  
}
