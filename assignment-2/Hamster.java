public class Hamster extends Animals{

    String name;
    int length;

    public Hamster(String name, int length){
        super(name, length, true);        // calls super class
    }

    public String seeGnawing(){
        return getName() + " makes a voice: ngkkrit.. ngkkrrriiit \n" +
                "Back to the office!\n\n"; 
    }

    public String orderToRunInWheel(){
        return getName() + " makes a voice: trrr.... trrr... \n" +
                "Back to the office!\n\n";
    
    }
    
    
}