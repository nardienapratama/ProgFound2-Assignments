import java.util.Scanner;

public class Parrot extends Animals{

    String name;
    int length;

    public Parrot(String name, int length){
        super(name, length, true);        // calls super class
    }

    public String orderToFly(){
        return "Parrot " + getName() + " flies!\n" +
                getName() + " makes a voice: FLYYYY..... \n" +
                "Back to the office!\n\n"; 
    }

    public void doConvo(){
        Scanner input = new Scanner(System.in);
        System.out.print("You say: ");
        String convoinput = input.nextLine();
        String convoinputcapital = convoinput.toUpperCase();
        System.out.println(getName() + " says: " + convoinputcapital + 
                            "\nBack to the office!\n\n");
    }

    public String doNothing(){
        return getName() + " says: HM? \n" + "Back to the office!\n\n";
    }
   
    
}