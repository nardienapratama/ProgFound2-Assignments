import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Collections;

public class Board extends JFrame {

    //private JPanel cardPanel;


    private List<Card> cards;
    private Card selectedCard;
    public static Card c1 = null;
    private static Card c2 = null;
    private Timer t;
    private boolean isSelected = false;
    public static boolean pressable = true;
    public static int totalAttempt = 0;
    public static JLabel attemptLabel = new JLabel("Attempt : " + Board.totalAttempt);
    private String a = "";


    public Board() {
        int cardpairs = 10;
        List<Card> cardsList = new ArrayList<Card>();
        List<Integer> cardVals = new ArrayList<Integer>();

        // proses inisialisasi card value
        String[] imageString = {"1.png", "2.png", "3.png", "4.png", "5.png", "6.png",
                "7.png", "8.png", "9.png", "10.png", "11.png", "12.png",
                "13.png", "14.png", "15.png", "16.png", "17.png", "18.png"};

        Collections.shuffle(Arrays.asList(imageString));

        for (int a = 0; a < cardpairs; a++) {
            cardsList.add(makeNewCard(a, imageString[a]));
            cardsList.add(makeNewCard(a, imageString[a]));
        }
//        System.out.println(cardsList.size());
        Collections.shuffle(cardsList);

//        for (int val : cardVals) {
//            Card c = new Card(val, );
//            c.addActionListener(new ActionListener() {
//                public void actionPerformed(ActionEvent ae) {
//                    selectedCard = c;
//                    doTurn();   // when clicked, it will turn
//                }
//            });
//            cardsList.add(c);
//        }

        this.cards = cardsList;
        System.out.println("isi cards: " + this.cards.size());
        // set up timer
        t = new Timer(750, new ActionListener() { // whilst timer goes, it will do actionlistener
            public void actionPerformed(ActionEvent ae) {
                checkCards();
            }
        });

        t.setRepeats(false);

        // set up the board
        Container pane = getContentPane();
        pane.setLayout(new GridLayout(4, 5));
        for (Card c : cards) {
            pane.add(c);
        }
        setTitle("Memory Match");
    }

    public Card makeNewCard(int value, String image) {
        Card c = new Card(value, image);
        c.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                selectedCard = c;
                doTurn();   // when clicked, it will turn
            }
        });
        return c;
    }

    public void setIsSelected(boolean val) {
        this.isSelected = val;
    }

    public boolean getIsSelected() {
        return this.isSelected;
    }

    public void doTurn() {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.setIcon(selectedCard.getMainImage());
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.setIcon(selectedCard.getMainImage());
            t.start();

        }
    }

    public void pairing() {
        if (Board.c1 != null && Board.c2 != null) {
            this.totalAttempt += 1;
//            Board.attemptLabel.setText("Attempt: " + Board.totalAttempt);
//            t.start();
        }
    }

    public void checkCards() {
        if (c1.getId() == c2.getId()) { // if cards match
            c1.setEnabled(false); // disables button
            c2.setEnabled(false);
            c1.setMatched(true); // flags the button as having been matched
            c2.setMatched(true);

            pairing();
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win! Your attempt : " + Board.totalAttempt,
                        "Congratulations!", JOptionPane.INFORMATION_MESSAGE);
                System.exit(0);
            }
        } else {
            c1.setIcon(Card.bgImage); // 'hides' the text
            c2.setIcon(Card.bgImage);
        }
        c1 = null;  // reset c1 and c2
        c2 = null;
    }

    // public void match(Card selectedcard){
    //     this.setEnabled(false);
    //     selectedcard.setEnabled(false);
    //     Board.c1 = null;
    //     Board.c2 = null;
    // }

    // public void notMatch(Card selectedCard){
    //     this.setIcon(bgImage);
    //     selectedCard.setIcon(bgImage);
    //     isSelected = false;
    //     selectedCard.setIsSelected(false);
    //     Board.c1 = null;
    //     Board.c2 = null;
    // }

    public boolean isGameWon() {
        for (Card c : this.cards) {
            if (c.getMatched() == false) {
                return false;
            }
        }
        return true;
    }
}