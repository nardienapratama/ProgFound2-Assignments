// import com.sun.javafx.scene.traversal.WeightedClosestCorner;

public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms

    // TODO Complete me!
    WildCat cat;
    TrainCar next;
    
    public TrainCar(WildCat cat) {          // if there is no car yet 
        // TODO Complete me!
        this.cat = cat;
        this.next = null;
    }
    
    public TrainCar(WildCat cat, TrainCar next) {       // if there is a car after
        // TODO Complete me!
        this.cat = cat;
        this.next = next;
    }

    public double computeTotalWeight() {
        // TODO Complete me!
        if(next==null){                         // if there is no car after
            return this.cat.weight + EMPTY_WEIGHT;
        }
        else{
            return this.cat.weight + EMPTY_WEIGHT + this.next.computeTotalWeight();    // adds the current weight and the weight of the next car
        }
    }

    public double computeTotalMassIndex() {
        // TODO Complete me!
        if(next==null){
            return this.cat.computeMassIndex(); 
        }
        else{
            return this.cat.computeMassIndex() + this.next.computeTotalMassIndex();
        }
    }

    public void printCar() {
        // TODO Complete me!
        if(this.next==null){
            System.out.print("--(" + this.cat.name + ")");
        }
        else{
            System.out.print("--(" + this.cat.name + ")");
            this.next.printCar();
        }
    }
}
