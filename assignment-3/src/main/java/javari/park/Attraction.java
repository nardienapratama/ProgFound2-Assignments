package javari.park;
import javari.animal.Animal;
import java.util.ArrayList;
import java.util.List;


public class Attraction implements SelectedAttraction{

    private String name;    // name of the animal - whale, cat
    private String type;    // type of attraction
    private List<Animal> performers = new ArrayList<>();    // list of performers

    public Attraction(String name, String type){
        this.name = name;
        this.type = type;
        this.performers = performers;
    }
    public String getName(){
        return this.name;
    }
    public String getType(){
        return this.type;
    }

    public List<Animal> getPerformers(){
        return this.performers;
    }

    public boolean addPerformer(Animal performer){
        if(performer.isShowable()){
            performers.add(performer);
        return true;
        }
        else{
            return false;
        }
    }

}

