import java.util.Random;

public class Cat extends Animals{

    String name;
    int length;

    public Cat(String name, int length){
        super(name, length, true);                // calls super class
    }

    public String brushCatFur(){            // if input is 1:brush the fur
        return "Time to clean " + getName() + "'s fur \n" +
                getName() + " makes a voice: Nyaaan...." +
                "\nBack to the office!\n\n";
    }

    public String cuddle(){                 // if input is 2:cuddle
        String[] sounds = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        int soundsNum = new Random().nextInt(sounds.length);

        return getName() + " makes a voice: " + sounds[soundsNum] +
                "\nBack to the office!\n\n";
    }


}