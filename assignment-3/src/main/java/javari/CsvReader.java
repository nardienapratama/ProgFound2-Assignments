package javari;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * This class represents the base class for reading all lines from a text
 * file that contains CSV data.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author TODO If you make changes in this class, please write your name here
 *     and describe the changes in the comment block
 */
public class CsvReader {

    public static final String COMMA = ",";

    private final Path file;
    protected final List<String> lines;

    /**
     * Defines the base constructor for instantiating an object of
     * {@code CsvReader}.
     *
     * @param file  path object referring to a CSV file
     * @throws IOException if given file is not present or cannot be read
     *     properly
     */
    // public CsvReader(Path file) throws IOException {
    //     this.file = file;
    //     this.lines = Files.readAllLines(this.file, StandardCharsets.UTF_8);
    // }

    String[] filenames = {"animals_categories.csv", "animals_attractions.csv", "animals_records.csv"};

    String filedirectory = "./../../../../data";    // relative addressing
    protected String[][][] alltemp = new String[3][][]; // 3 files, amount of lines, the tokens in each line

    public CsvReader(){
        Scanner input = new Scanner(System.in);

        try{
            for(int i=0; i<3; i++){
                File inputFile = new File(filedirectory + filenames[i]);  // the directory name added by the file name
                Scanner data = new Scanner(inputFile);
                ArrayList<String> temp = new ArrayList<>();
                while(data.hasNextLine()){      //"Hamster, mammals, Explore the Mammals\nLion, mammals"
                    temp.add(data.nextLine());  //[(1st line), (2nd line), (3rd line)]
                }
                String[][] temp2 = new String[temp.size()][]; // the first dimension's size has to be specified, the second doesn't have to be
                for(int j=0; j<temp.size();j++){    //temp2
                    temp2[j] = temp.get(j).split(","); // temp2 = [["Hamster", "Circles"], ["Eagle" , "Circles"]]
                }
                alltemp[i] = temp2; // stores data from temp2 into alltemp (otherwise it gets overwritten)

            }
            System.out.println("Success... System is populating data...");

        }
        catch(Exception e){
            if(e instanceof FileNotFoundException){
                System.out.println("File not found or incorrect file!\nPlease provide the source data path: ");
                filedirectory = input.nextLine(); // changing filedirectory into a new input
                System.out.println("... Loading... ");
            }
        }

    }



        /**
     * Returns all line of text from CSV file as a list.
     *
     * @return
     */
    public List<String> getLines() {
        return lines;
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    // public long countValidRecords(){

    // }

    // /**
    //  * Counts the number of invalid records from read CSV file.
    //  *
    //  * @return
    //  */
    // public long countInvalidRecords(){

    // }

}
