package javari.animal;

public abstract class Animal{

    /**
 * This class represents common attributes and behaviours found in all animals
 * in Javari Park.
 *
 * @author Programming Foundations 2 Teaching Team
 * @author Nardiena Althafia Pratama
 *     and describe the changes in the comment block
 */
    // public static final List<String> MAMMALS = Arrays.asList("Hamster", "Lion", "Cat", "Whale");
    // public static final List<String> AVES = Arrays.asList("Eagle", "Parrot");
    // public static final List<String> REPTILES = Arrays.asList("Snake");
    private Integer id;
    private String type;
    private String name;
    private Body body;
    private Condition condition;

    public Animal(Integer id, String type, String name, Gender gender, double length, 
                double weight, Condition condition) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.body = new Body(length, weight, gender);
        this.condition = condition;
        }
    
    public Integer getId() {
    return id;
    }

    public String getType() {
    return type;
    }

    public String getName() {
    return name;
    }

    /**
    * Returns {@code Gender} identification of the animal.
    *
    * @return
    */
    public Gender getGender() {
    return body.getGender();
    }

    public double getLength() {
    return body.getLength();
    }

    public double getWeight() {
    return body.getWeight();
    }

    /**
    * Returns {@code Condition} of the animal.
    *
    * @return
    */
    public Condition getCondition() {
    return condition;
    }

    /**
    * Determines whether the animal can perform their attraction or not.
    *
    * @return
    */
    public boolean isShowable() {
    return condition == Condition.HEALTHY && specificCondition();
    }
    protected abstract boolean specificCondition(); //abstract method





}