public class Cage{
    Animals animal;
    String type;
    String location;
    Cage x;

    public Cage(Animals animal, String type, String location){  // what animal, A/B/C, indoor/outdoor
        this.animal = animal;
        this.type = type;
        this.location = location;
    }

    public static Cage determineABC(Animals animal){
        if(animal instanceof Cat|| animal instanceof Hamster || animal instanceof Parrot){ 
            if(animal.getLength() < 45){
                Cage cage = new Cage(animal,"A", "indoor");
				return cage;
            }
            else if (animal.getLength()>= 45 && animal.getLength()< 60){
                Cage cage = new Cage(animal, "B", "indoor");
                return cage;
            }
            else{
                Cage cage = new Cage(animal, "C", "indoor");
                return cage;
            }
        }

        if(animal instanceof Lion|| animal instanceof Eagle){ 
            if(animal.getLength() < 75){
                Cage cage = new Cage(animal,"A", "outdoor");
                return cage;
            }
            else if (animal.getLength()>= 75 && animal.getLength()< 90){
                Cage cage = new Cage(animal, "B", "outdoor");
                return cage;
            }
            else{
                Cage cage = new Cage(animal, "C", "outdoor");
                return cage;
            }
        }
		return null;
    }

    public Animals getAnimal(){
        return this.animal;
    }

    public String getType(){
        return this.type;
    }

    public String getLocation(){
        return this.location;
    }
}





    /*public 




    public String getLocation(){
        return this.location;
    }
    
    

    public String doremi(String asd){
        return asd;
    }*/


