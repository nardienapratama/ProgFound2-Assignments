import java.util.Random;

public class Eagle extends Animals{

    String name;
    int length;

    public Eagle(String name, int length){
        super(name, length, false);        // calls super class
    }

    public String orderToFly(){
        return getName() + " makes a voice: kwaakk... \n" +
                "You hurt! \n" + "Back to the office!\n\n"; 
    }

}