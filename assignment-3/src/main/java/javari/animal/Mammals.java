package javari.animal;
//import javari.animal.Animal;

public class Mammals extends Animal{

    //pregnant, female/not, specific conditions

    private boolean pregnant;
    private boolean female;
    private String specialcond;

    public Mammals(Integer id, String type, String name, Gender gender, double length,
    double weight, String specialcond, Condition condition){
        super(id, type, name, gender, length, weight, condition);
        this.specialcond = specialcond;
        this.pregnant = false;
        this.female = false;
    }

    // to check their conditions - determines whether they can do attractions
    protected boolean specificCondition(){
        if(this.getType().equalsIgnoreCase("lion")){
            if(this.getGender() == Gender.FEMALE ){
                this.female = true;
            }
        }if(specialcond.equalsIgnoreCase("pregnant")){
            this.pregnant = true;
        }return this.female && this.pregnant; // returns these boolean values
    }


}