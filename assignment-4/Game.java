import java.awt.Dimension;
import javax.swing.JFrame;
import javax. swing.*;

public class Game{
    public static void main(String[] args){
        // JFrame f = new JFrame(); // creating instance of JFrame

        // JButton b = new JButton("click");   // creating instance of JButton
        // b.setBounds(130,100,100,40);    // x axis, y axis, width, height

        // f.add(b);       // add the button the JFrame

        // JLabel label = new JLabel("Memory Card Game");
        // f.getContentPane().add(label);

        // f.setSize(500,500); // 400 width and 500 height
        // f.setLayout(null);  // using no layout managers
        // f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // f.pack();
        // f.setVisible(true); // making the frame visible



        Board b = new Board();
        b.setPreferredSize(new Dimension(500,500)); // need to use this instead of setSize
        b.setLocation(500, 250);
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }
}